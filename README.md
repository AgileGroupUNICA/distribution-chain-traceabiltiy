# README #

This repository is part of the document "
A blockchain based system for transparency and traceability in food
distribution chain in a Smart Tourism Region contextn" by Gavina Baralla et al.

### Summary ##
This repository will contain the implementation of the three smart contracts of the presented system:
* ProductionChain
* DistributionChain
* Certification Chain
